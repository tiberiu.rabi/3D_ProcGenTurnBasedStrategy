using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ScoreAndExperience : MonoBehaviour
{
    public TMP_Text ScoreText;
    public TMP_Text UpgradesText;

    public int Score = 0;
    public int TotalUpgradePoints = 0;
    public int UpgradePointsSpent = 0;

    private void Start()
    {
        ScoreText = GameObject.Find("Canvas").transform.Find("TurnUI").transform.Find("ScoreText").GetComponent<TMP_Text>();
        UpgradesText = GameObject.Find("Canvas").transform.Find("TurnUI").transform.Find("UpgradeText").GetComponent<TMP_Text>();
        AddScore(0);
    }

    public void AddScore(int toAdd)
    {
        Score += toAdd;
        ScoreText.text = "Score:" + Score;
        ScoreUpgradesStatic.Score = Score;
        TotalUpgradePoints = Mathf.FloorToInt(Score / 1000f);
        UpgradesText.text = "UPoints:" + (TotalUpgradePoints - UpgradePointsSpent);
        ScoreUpgradesStatic.AvailableUpgradePoints = (TotalUpgradePoints - UpgradePointsSpent);
    }

    public void UsedUpgradePoints(int UPointsSpent)
    {
        UpgradePointsSpent += UPointsSpent;
        AddScore(0); // update UI
    }
}
public static class ScoreUpgradesStatic
{
    public static int Score { get; set; }
    public static int AvailableUpgradePoints { get; set; }

}