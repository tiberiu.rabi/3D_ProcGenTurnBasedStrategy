using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class TileMeshGenerator : MonoBehaviour
{
    Mesh mesh;
    private Vector3[] VertsNoRot = { new Vector3(-0.5f, 0, 0.866f), new Vector3(-0.5f, 0, 1.066f), new Vector3(0.5f, 0, 1.066f), new Vector3(0.615f, 0, 1.066f) };
    private Vector3[] FinalVerts = new Vector3[25];
    private Vector3[] Normals;
    private int[] FinalTris = { 0, 1, 5, 0, 5, 9, 0, 9, 13, 0, 13, 17, 0, 17, 21, 0, 21, 1, //int tris
    1,2,3,1,3,5,5,6,7,5,7,9,9,10,11,9,11,13,13,14,15,13,15,17,17,18,19,17,19,21,21,22,23,21,23,1, //extensions but no corners        
    5,3,4,5,4,6,9,7,8,9,8,10,13,11,12,13,12,14,17,15,16,17,16,18,21,19,20,21,20,22,1,23,24,1,24,2 //corner tris
    };
    private int[] InVerts = { 1, 5, 9, 13, 17, 21 };
    private int[] ExtVerts = { 2, 3, 6, 7, 10, 11, 14, 15, 18, 19, 22, 23 };
    private int[] CornerVerts = { 4, 8, 12, 16, 20, 24 };

    // Start is called before the first frame update
    void Start()
    {
        int VectorIndex = 0;
        FinalVerts[0] = new Vector3(0, 0, 0);
        for(int x1 = 0; x1 < 6; x1++)
        {
            for(int x2 = 0; x2 < 4; x2++)
            {
                VectorIndex += 1;
                FinalVerts[VectorIndex] = Quaternion.AngleAxis(x1*60f, Vector3.up) * VertsNoRot[x2];
            }
        }
        Normals = new Vector3[FinalVerts.Length];
        InitNormals(Vector3.right);
        mesh = new Mesh();
        mesh.name = "ProcHexMesh";
        mesh.vertices = FinalVerts;
        mesh.triangles = FinalTris;
        mesh.normals = Normals;
        transform.GetComponent<MeshFilter>().mesh = mesh;
    }

    private void InitNormals(Vector3 value)
    {
        for(int x1 = 0; x1 < Normals.Length; x1++)
        {
            Normals[x1] = value;
        }
    }

    public void UpdateSelfMesh(int[] Heights, int SelfHeight, float HeightStep)
    {
        for(int VecIndex = 0; VecIndex < FinalVerts.Length; VecIndex++)
        {
            FinalVerts[VecIndex].y = 0;
        }
        int VertIndex = 0;
        for(int x1 = 0; x1 < 6; x1++)
        {
            int Diff = 0;
            int ExtraCornerDiff = 0;

            if (Heights[x1] != 0)
            {
                Diff = SelfHeight - Heights[x1];
                if (Heights[(x1 + 1)%6] != 0)
                {
                    ExtraCornerDiff = SelfHeight - Heights[(x1 + 1) % 6];
                }
            }
            float MoveY = Diff * -HeightStep / 2;
            FinalVerts[ExtVerts[VertIndex]].y = FinalVerts[ExtVerts[VertIndex]].y + MoveY;
            VertIndex += 1;
            FinalVerts[ExtVerts[VertIndex]].y = FinalVerts[ExtVerts[VertIndex]].y + MoveY;
            VertIndex += 1;

            FinalVerts[CornerVerts[x1]].y = FinalVerts[CornerVerts[x1]].y + (Diff+ExtraCornerDiff) * -HeightStep/3;
        }

        mesh = new Mesh();
        mesh.name = "ProcHexMesh";
        mesh.vertices = FinalVerts;
        mesh.triangles = FinalTris;
        mesh.normals = Normals;
        transform.GetComponent<MeshFilter>().mesh = mesh;
    }
}

/*
Vector3[] newVertices = { new Vector3(0, 0, 0), new Vector3(1, 0, 0), new Vector3(0.5f, 0, -0.866f), new Vector3(-0.5f, 0, -0.866f), new Vector3(-1, 0, 0), new Vector3(-0.5f, 0, 0.866f), new Vector3(0.5f, 0, 0.866f),
new Vector3(1.2f, 0, 0), new Vector3(0.6f, 0, -1.0392f), new Vector3(-0.6f, 0, -1.0392f), new Vector3(-1.2f, 0, 0), new Vector3(-0.6f, 0, 1.0392f), new Vector3(0.6f, 0, 1.0392f)};
int[] newTriangles = { 0, 1, 2, 0, 2, 3, 0, 3, 4, 0, 4, 5, 0, 5, 6, 0, 6, 1, //interiour tris
1,7,2,7,8,2,2,8,3,8,9,3,3,9,4,9,10,4,4,10,5,10,11,5,5,11,6,11,12,6,6,12,1,12,7,1}; //exteriour tris
*/