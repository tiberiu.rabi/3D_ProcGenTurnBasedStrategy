using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Selector : MonoBehaviour
{
    [SerializeField]
    private GameObject SelectHexOutline;
    private float MinPrecision = 0.05f;

    private GameObject InstSelector;
    private GameObject SelectedTile;
    private GameObject SelectedChild;
    private GameObject SelectedEnemy;

    private Vector3 MouseLastPos;
    private float TimeMouseStoppedMoving;
    private bool MouseStoppedMoving = false;

    private int UILayer;

    private void Start()
    {
        UILayer = LayerMask.NameToLayer("UI");
        MouseLastPos = Input.mousePosition;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0) && !IsPointerOverUIElement())
        {
            float TempDist = 12f;
            Vector3 PointInit = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, TempDist));
            while (Mathf.Abs(PointInit.y) > MinPrecision)
            {
                TempDist += PointInit.y;
                PointInit = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, TempDist));
            }
            SelectedTile = MapGenScriptStatics.MapGenObject.GetComponent<MapFindTile>().FindChildByPosNotNull(PointInit);
            SelectedTile.GetComponent<TileStats>().ShowStats();
            DestroySelector();
            InstSelector = Instantiate(SelectHexOutline, SelectedTile.transform.position, Quaternion.identity);
            if (SelectedTile.transform.childCount == 2) // it has enemy or player units
            {
                SelectedChild = SelectedTile.transform.GetChild(1).gameObject;
                if (SelectedChild.tag == "PlayerUnit")
                {
                    InstSelector.GetComponent<MeshRenderer>().material.color = Color.blue;
                    ShowPlayerUnitStats();
                }
                else
                {
                    InstSelector.GetComponent<MeshRenderer>().material.color = Color.red;
                    SelectedEnemy = SelectedChild;
                    SelectedChild = null;
                    ShowEnemyUnitStats();
                }
            }
            else
            {
                GameObject.Find("Canvas").transform.Find("UnitInfo").GetComponent<UnitInfoShow>().HideWindow();
                GameObject.Find("Canvas").transform.Find("EnemyInfo").GetComponent<UnitInfoShow>().HideWindow();
            }
        }

        if (Input.GetMouseButtonUp(1) && SelectedChild!=null && SelectedChild.transform.tag == "PlayerUnit") //Action with selected player unit
        {
            float Dist2 = 12f;
            Vector3 PointInit = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Dist2));
            while (Mathf.Abs(PointInit.y) > MinPrecision)
            {
                Dist2 += PointInit.y;
                PointInit = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Dist2));
            }
            GameObject TargetTile = MapGenScriptStatics.MapGenObject.transform.GetComponent<MapFindTile>().FindChildByPosNotNull(PointInit);
            if (TargetTile.transform.childCount != 2) // has no unit of any kind on it
            {
                SelectedChild.GetComponent<UnitCore>().MoveToTile(TargetTile);
                ShowPlayerUnitStats();
                DestroySelector();
                InstSelector = Instantiate(SelectHexOutline, SelectedChild.transform.parent.transform.position, Quaternion.identity);
            }
            else // has unit, player or enemy
            {
                if(TargetTile.transform.GetChild(1).transform.tag == "EnemyUnit")
                {
                    SelectedChild.GetComponent<UnitCore>().Attack(TargetTile.transform.GetChild(1).gameObject);
                    ShowPlayerUnitStats();
                    DestroySelector();
                    InstSelector = Instantiate(SelectHexOutline, SelectedChild.transform.parent.transform.position, Quaternion.identity);
                    SelectedEnemy = TargetTile.transform.GetChild(1).gameObject;
                    ShowEnemyUnitStats();
                }
                else
                {

                }
            }
        }

        if(Input.mousePosition == MouseLastPos) // mouse stationary
        {
            if(MouseStoppedMoving == false)
            {
                TimeMouseStoppedMoving = Time.realtimeSinceStartup;
            }
            MouseStoppedMoving = true;
            if(MouseStoppedMoving == true)
            {
                if(Mathf.Abs(TimeMouseStoppedMoving - Time.realtimeSinceStartup) > 2) // mouse hasn't moved in 2 sec
                {
                    float Dist2 = 12f;
                    Vector3 PointInit = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Dist2));
                    while (Mathf.Abs(PointInit.y) > MinPrecision)
                    {
                        Dist2 += PointInit.y;
                        PointInit = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Dist2));
                    }
                    GameObject TargetTile = MapGenScriptStatics.MapGenObject.transform.GetComponent<MapFindTile>().FindChildByPosNotNull(PointInit);
                    if (TargetTile.transform.childCount == 2 && TargetTile.transform.GetChild(1).transform.tag == "EnemyUnit" && SelectedChild!=null)
                    {
                        GameObject.Find("Canvas").transform.Find("AttackStats").GetComponent<ShowAttackOutcome>().ShowStats(SelectedChild, TargetTile.transform.GetChild(1).gameObject);
                    }
                }
            }
        }
        else
        {
            MouseStoppedMoving = false;
            GameObject.Find("Canvas").transform.Find("AttackStats").GetComponent<ShowAttackOutcome>().Dissappear();
        }
        MouseLastPos = Input.mousePosition;
    }

    private void ShowPlayerUnitStats()
    {
        UnitInfoShow UnitShowScript = GameObject.Find("Canvas").transform.Find("UnitInfo").GetComponent<UnitInfoShow>();
        if (SelectedChild != null && SelectedChild.transform.tag == "PlayerUnit" && UnitShowScript!=null)
        {
            UnitShowScript.ShowUnitStats(SelectedChild);
        }
    }

    private void ShowEnemyUnitStats()
    {
        UnitInfoShow UnitShowScript = GameObject.Find("Canvas").transform.Find("EnemyInfo").GetComponent<UnitInfoShow>();
        if(SelectedEnemy != null && SelectedEnemy.transform.tag == "EnemyUnit" && UnitShowScript != null)
        {
            UnitShowScript.ShowUnitStats(SelectedEnemy);
        }
    }

    private void DestroySelector()
    {
        if (InstSelector != null)
        {
            Destroy(InstSelector);
        }
    }
    public bool IsPointerOverUIElement()
    {
        return IsPointerOverUIElement(GetEventSystemRaycastResults());
    }

    private bool IsPointerOverUIElement(List<RaycastResult> eventSystemRaysastResults)
    {
        for (int index = 0; index < eventSystemRaysastResults.Count; index++)
        {
            RaycastResult curRaysastResult = eventSystemRaysastResults[index];
            if (curRaysastResult.gameObject.layer == UILayer)
                return true;
        }
        return false;
    }

    static List<RaycastResult> GetEventSystemRaycastResults()
    {
        PointerEventData eventData = new PointerEventData(EventSystem.current);
        eventData.position = Input.mousePosition;
        List<RaycastResult> raysastResults = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventData, raysastResults);
        return raysastResults;
    }
}

/*
 * 

            if (LastSelectedTile == null)
            {
                float Dist2 = 12f;
                Vector3 PointInit = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Dist2));
                while (Mathf.Abs(PointInit.y) > MinPrecision)
                {
                    Dist2 += PointInit.y;
                    PointInit = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Dist2));
                }
                LastSelectedTile = MapGenScriptStatics.MapGenObject.transform.GetComponent<MapFindTile>().FindChildByPosNotNull(PointInit);
                LastSelectedTile.transform.GetComponent<TileStats>().ShowStats();
                DestroySelector();
                InstSelector = Instantiate(SelectHexOutline, LastSelectedTile.transform.position, Quaternion.identity);
            }
            else
            {
                float Dist2 = 12f;
                Vector3 PointInit = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Dist2));
                while (Mathf.Abs(PointInit.y) > MinPrecision)
                {
                    Dist2 += PointInit.y;
                    PointInit = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Dist2));
                }
                GameObject NewSelectedTile = MapGenScriptStatics.MapGenObject.transform.GetComponent<MapFindTile>().FindChildByPosNotNull(PointInit);
                if(NewSelectedTile != LastSelectedTile)
                {
                    LastSelectedTile = NewSelectedTile;
                    LastSelectedTile.transform.GetComponent<TileStats>().ShowStats();
                    DestroySelector();
                    InstSelector = Instantiate(SelectHexOutline, LastSelectedTile.transform.position, Quaternion.identity);
                }
                else // same tile clicked more times.
                {
                    if(LastSelectedTile.transform.childCount == 2)
                    {
                        SelectedChild = LastSelectedTile.transform.GetChild(1).gameObject;
                        DestroySelector();
                        InstSelector = Instantiate(SelectHexOutline, LastSelectedTile.transform.GetChild(1).transform.position, Quaternion.identity);
                        ShowPlayerUnitStats();
                    }
                }
            }
*/