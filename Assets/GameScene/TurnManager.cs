using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TurnManager : MonoBehaviour
{
    public int CurrentTurn;
    private TMP_Text TurnUiElement;

    private EnemySpawner EnemySpawnerScript;
    private EnemyActionTaker EnemyActionScript;
    private PlayerUnitSpawner PlayerSpawnerScript;
    private PlayerUnitController PlayerActionScript;


    private void Start()
    {
        CurrentTurn = 0;
        TurnUiElement = GameObject.Find("Canvas").transform.Find("TurnUI").transform.Find("TurnText").GetComponent<TMP_Text>();
        TurnUiElement.text = "Turn: " + CurrentTurn;
        EnemySpawnerScript = GameObject.Find("EnemyManager").GetComponent<EnemySpawner>();
        EnemyActionScript = GameObject.Find("EnemyManager").GetComponent<EnemyActionTaker>();
        PlayerSpawnerScript = GameObject.Find("SelectorObj").GetComponent<PlayerUnitSpawner>();
        PlayerActionScript = GameObject.Find("SelectorObj").GetComponent<PlayerUnitController>();
    }

    public void SpawnUnits()
    {
        EnemySpawnerScript.SpawnInitialEnemies();
        PlayerSpawnerScript.SpawnInitialPlayerUnits();
    }

    public void EndPlayerTurn()
    {
        EnemyActionScript.RefreshUnitsAP();
        EnemyActionScript.TakeActions();
    }

    public void EndEnemyTurn()
    {
        CurrentTurn += 1;
        TurnUiElement.text = "Turn: " + CurrentTurn;
        PlayerActionScript.RefreshUnitsAP();
    }
}
