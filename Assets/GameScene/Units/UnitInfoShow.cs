using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class UnitInfoShow : MonoBehaviour
{
    [SerializeField]
    private Sprite MountedIcon;
    [SerializeField]
    private Sprite AfootIcon;


    private GameObject UnitNameObj;
    private TMP_Text UnitTypeText;
    private Scrollbar APBar;
    private TMP_Text APText;
    private Scrollbar UnitCountBar;
    private TMP_Text UnitCountText;
    private Scrollbar SupplyCountBar;
    private TMP_Text SupplyCountText;
    private Image MovementTypeImage;
    private TMP_Text MovementTypeText;
    private TMP_Text AttackCostText;
    private TMP_Text AttackChanceText;
    private TMP_Text RetaliationChanceText;
    private TMP_Text DefensePointsText;
    private TMP_Text AttackAfootText;
    private TMP_Text AttackMountedText;
    private TMP_Text PenetationPointsText;

    private GameObject LastSelectedUnit;
    private bool IsHidden;
    public bool PlayerUI;
    private Button UpgradeBtn;
    private Button SleepBtn;
    private Button ResupplyBtn;


    void Start()
    {
        UnitNameObj = transform.Find("UnitName").gameObject;
        UnitTypeText = transform.Find("UnitType").GetComponent<TMP_Text>();
        APBar = transform.Find("ActionPoints").transform.Find("Bar").GetComponent<Scrollbar>();
        APText = transform.Find("ActionPoints").transform.Find("Text").GetComponent<TMP_Text>();
        UnitCountBar = transform.Find("UnitCount").transform.Find("Bar").GetComponent<Scrollbar>();
        UnitCountText = transform.Find("UnitCount").transform.Find("Text").GetComponent<TMP_Text>();
        SupplyCountBar = transform.Find("SupplyCount").transform.Find("Bar").GetComponent<Scrollbar>();
        SupplyCountText = transform.Find("SupplyCount").transform.Find("Text").GetComponent<TMP_Text>();
        MovementTypeImage = transform.Find("MovementType").transform.Find("Icon").GetComponent<Image>();
        MovementTypeText = transform.Find("MovementType").transform.Find("Text").GetComponent<TMP_Text>();
        AttackCostText = transform.Find("AttackCost").transform.Find("Text").GetComponent<TMP_Text>();
        AttackChanceText = transform.Find("ChanceHit").transform.Find("Text").GetComponent<TMP_Text>();
        RetaliationChanceText = transform.Find("ChanceRetaliate").transform.Find("Text").GetComponent<TMP_Text>();
        DefensePointsText = transform.Find("DefensePoints").transform.Find("Text").GetComponent<TMP_Text>();
        AttackAfootText = transform.Find("AttackAfoot").transform.Find("Text").GetComponent<TMP_Text>();
        AttackMountedText = transform.Find("AttackMounted").transform.Find("Text").GetComponent<TMP_Text>();
        PenetationPointsText = transform.Find("PenPoints").transform.Find("Text").GetComponent<TMP_Text>();
        transform.GetComponent<RectTransform>().anchoredPosition = new Vector2(transform.GetComponent<RectTransform>().anchoredPosition.x, -290f);

        if(PlayerUI == true)
        {
            UpgradeBtn = transform.Find("UpgradeBtn").GetComponent<Button>();
            SleepBtn = transform.Find("SleepBtn").GetComponent<Button>();
            ResupplyBtn = transform.Find("ResupplyBtn").GetComponent<Button>();
        }
    }

    private void Update()
    {
        
    }

    public void SetNameChangeInteractive()
    {
        UnitNameObj.GetComponent<TMP_InputField>().interactable = true;
    }

    public void UpdateLastSelectedUnitName()
    {
        LastSelectedUnit.GetComponent<UnitCore>().UnitName = UnitNameObj.GetComponent<TMP_InputField>().text;
    }

    public void ShowUnitStats(GameObject x)
    {
        transform.GetComponent<RectTransform>().anchoredPosition = new Vector2(transform.GetComponent<RectTransform>().anchoredPosition.x, 290f);
        IsHidden = false;
        LastSelectedUnit = x;
        UnitCore PlayerUnitScript = x.GetComponent<UnitCore>();
        UnitNameObj.GetComponent<TMP_InputField>().text = "" + PlayerUnitScript.UnitName;
        UnitTypeText.text = "" + PlayerUnitScript.UnitType;
        APBar.size = ((PlayerUnitScript.CurrentActionPoints * 100f) / PlayerUnitScript.MaxActionPoints) / 100f;
        APBar.value = 0;
        APText.text = "" + PlayerUnitScript.CurrentActionPoints + "/" + PlayerUnitScript.MaxActionPoints;
        UnitCountBar.size = ((PlayerUnitScript.CurrUnitCount * 100f) / PlayerUnitScript.MaxUnitCount) / 100f;
        UnitCountBar.value = 0;
        UnitCountText.text = "" + PlayerUnitScript.CurrUnitCount + "/" + PlayerUnitScript.MaxUnitCount;
        SupplyCountBar.size = ((PlayerUnitScript.CurrSupply * 100f) / PlayerUnitScript.MaxSupply) / 100f;
        SupplyCountBar.value = 0;
        SupplyCountText.text = "" + PlayerUnitScript.CurrSupply + "/" + PlayerUnitScript.MaxSupply;
        MovementTypeImage.sprite = PlayerUnitScript.Mounted == true ? MountedIcon : AfootIcon;
        MovementTypeText.text = PlayerUnitScript.Mounted == true ? "Mounted" : "Afoot";
        AttackCostText.text = "" + PlayerUnitScript.AttackAPCost + "AP";
        AttackChanceText.text = "" + PlayerUnitScript.ChanceToHit + "%";
        RetaliationChanceText.text = "" + PlayerUnitScript.ChanceToRetaliate + "%";
        DefensePointsText.text = "" + PlayerUnitScript.DefensePoints;
        AttackAfootText.text = "" + PlayerUnitScript.AttackAgainstAfoot;
        AttackMountedText.text = "" + PlayerUnitScript.AttackAgainstMounted;
        PenetationPointsText.text = "" + PlayerUnitScript.PenetrationPoints;


        if (PlayerUI == true && IsHidden == false && LastSelectedUnit != null)
        {
            if (ScoreUpgradesStatic.AvailableUpgradePoints >= 2)
            {
                UpgradeBtn.interactable = true;
            }
            else
            {
                UpgradeBtn.interactable = false;
            }
            if (LastSelectedUnit.GetComponent<UnitCore>().CurrentActionPoints != LastSelectedUnit.GetComponent<UnitCore>().MaxActionPoints)
            {
                ResupplyBtn.interactable = false;
            }
            else
            {
                ResupplyBtn.interactable = true;
            }
        }
    }

    public void SleepUnit()
    {
        LastSelectedUnit.GetComponent<UnitCore>().WasteRemainingAp();
        ShowUnitStats(LastSelectedUnit);
    }

    public void UpgradeUnit()
    {
        int rand = Random.Range(0, 2);
        if(rand == 0)
        {
            LastSelectedUnit.GetComponent<UnitCore>().UpgradeUnitTo1();
            GameObject.Find("TurnManager").GetComponent<ScoreAndExperience>().UsedUpgradePoints(2);
            ShowUnitStats(LastSelectedUnit);
        }
        else
        {
            LastSelectedUnit.GetComponent<UnitCore>().UpgradeUnitTo2();
            GameObject.Find("TurnManager").GetComponent<ScoreAndExperience>().UsedUpgradePoints(2);
            ShowUnitStats(LastSelectedUnit);
        }
    }

    public void ResupplyUnit()
    {
        LastSelectedUnit.GetComponent<UnitCore>().Resupply();
        ShowUnitStats(LastSelectedUnit);
    }


    public void HideWindow()
    {
        transform.GetComponent<RectTransform>().anchoredPosition = new Vector2(transform.GetComponent<RectTransform>().anchoredPosition.x, -290f);
        IsHidden = true;
    }
}
