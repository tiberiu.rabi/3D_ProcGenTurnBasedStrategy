using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ShowAttackOutcome : MonoBehaviour
{
    [SerializeField]
    private Sprite Background;

    private Image SelfBackground;
    private TMP_Text HitChance;
    private TMP_Text MinMaxDamage;
    private TMP_Text RetalChance;
    private TMP_Text MinMaxRetalDamage;

    void Start()
    {
        SelfBackground = GetComponent<Image>();
        HitChance = transform.Find("ChanceToHit").transform.GetChild(0).GetComponent<TMP_Text>();
        MinMaxDamage = transform.Find("DamageRange").transform.GetChild(0).GetComponent<TMP_Text>();
        RetalChance = transform.Find("Retaliation chance").transform.GetChild(0).GetComponent<TMP_Text>();
        MinMaxRetalDamage = transform.Find("DamageRecievedRange").transform.GetChild(0).GetComponent<TMP_Text>();

        Dissappear();
    }

    public void ShowStats(GameObject AttackingUnit, GameObject DefendingUnit)
    {
        SelfBackground.enabled = true;
        SelfBackground.sprite = Background;
        for (int x1 = 0; x1 < transform.childCount; x1++)
        {
            transform.GetChild(x1).gameObject.SetActive(true);
        }
        HitChance.text = "Chance to hit:" + AttackingUnit.GetComponent<UnitCore>().GiveHitChanceAgainst(DefendingUnit) + "%";
        MinMaxDamage.text = "Min/Max damage dealt:" + AttackingUnit.GetComponent<UnitCore>().GiveSuccessfulAttackMinMaxDamage(DefendingUnit);
        RetalChance.text = "Chance the target will retaliate:" + DefendingUnit.GetComponent<UnitCore>().GiveRetaliationChanceAgainst(AttackingUnit) + "%";
        MinMaxRetalDamage.text = "Min/Max damage recieved if retaliation:" + DefendingUnit.GetComponent<UnitCore>().GiveSuccessfulAttackMinMaxDamage(AttackingUnit);
    }

    public void Dissappear()
    {
        SelfBackground.enabled = false;
        for (int x1 = 0; x1 < transform.childCount; x1++)
        {
            transform.GetChild(x1).gameObject.SetActive(false);
        }
    }
}
