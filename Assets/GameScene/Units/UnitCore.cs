using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class UnitCore : MonoBehaviour
{
    public bool PlayerUnit;
    public bool Selected;
    public GameObject SelfTile;

    //Base things
    public string UnitName;
    public bool Mounted;
    public int UnitType;

    //Action points
    public int MaxActionPoints;
    //[DisplayWithoutEdit()]
    public int CurrentActionPoints;

    //supply
    public int MaxSupply;
    //[DisplayWithoutEdit()]
    public int CurrSupply;

    //Health stats
    public int UnitHealth;
    public int MaxUnitCount;
    //[DisplayWithoutEdit()]
    public int CurrUnitCount;
    //[DisplayWithoutEdit()]
    public int CurrTotalHealth;

    //New Attack stats
    public int AttackAPCost;
    public int ChanceToHit;
    public int ChanceToRetaliate;
    public int DefensePoints;
    public int AttackAgainstAfoot;
    public int AttackAgainstMounted;
    public int PenetrationPoints;

    public AudioClip MoveSound;
    public AudioClip AttackSound;



    //attack calculation is: (ThisCurrUnitCount * ThisAttackAgainstX) * (1 - (EnemyDefensePoints - ThisPenetrationPoints)*0.05);

    private void Start()
    {
        InstUnit();
    }

    public void InstUnit()  
    {
        CurrentActionPoints = MaxActionPoints;
        CurrUnitCount = MaxUnitCount;
        CurrTotalHealth = CurrUnitCount * UnitHealth;
        CurrSupply = MaxSupply;
        SelfTile = transform.parent.gameObject;
        if(PlayerUnit == true)
        {
            UnitName = GenerateRandomName();
        }
        else
        {
            UnitName = GenerateRandomEnemyName();
        }
    }

    public void MoveToTile(GameObject Tile)
    {
        int Cost = Tile.GetComponent<TileStats>().MoveIntoCost(Mounted, SelfTile.GetComponent<TileStats>().Height);
        if(CurrentActionPoints - Cost >= 0 && CheckIfNeighbor(Tile) == true && Tile.transform.childCount < 2)
        {
            CurrentActionPoints -= Cost;
            MoveToTileConfirmed(Tile);
        }
    }

    public bool CheckIfNeighbor(GameObject Tile)
    {
        Vector2 ThisPos = new Vector2(this.transform.parent.transform.position.x, this.transform.parent.transform.position.z);
        Vector2 TargetPos = new Vector2(Tile.transform.position.x, Tile.transform.position.z);
        float Distance = Vector2.Distance(ThisPos,TargetPos);
        //Debug.LogError(Distance);
        if(Distance>2.1315f && Distance < 2.1318f)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private void MoveToTileConfirmed(GameObject Tile)
    {
        if(transform.GetComponent<AudioSource>().isPlaying == false)
        {
            transform.GetComponent<AudioSource>().PlayOneShot(MoveSound, StaticSettings.SoundsVolume);
        }
        SelfTile = Tile;
        transform.position = SelfTile.transform.position;
        transform.parent = Tile.transform;
        
    }

    public void Attack(GameObject Target)
    {
        if(CurrentActionPoints - AttackAPCost >= 0) // check if unit can attack
        {
            CurrentActionPoints -= AttackAPCost;
            AttackConfirmed(Target);
        }
    }

    private void AttackConfirmed(GameObject Target)
    {
        if (transform.GetComponent<AudioSource>().isPlaying == false)
        {
            transform.GetComponent<AudioSource>().PlayOneShot(AttackSound, StaticSettings.SoundsVolume);
        }
        int AttackRoll = Random.Range(1, 101);
        int BonusChanceToHit = 0;
        if(SelfTile.GetComponent<TileStats>().Height > Target.transform.parent.GetComponent<TileStats>().Height)
        {
            BonusChanceToHit = 5;
        }
        else if(SelfTile.GetComponent<TileStats>().Height < Target.transform.parent.GetComponent<TileStats>().Height)
        {
            BonusChanceToHit = -5;
        }
        if(AttackRoll < (ChanceToHit + BonusChanceToHit)) // successful attack
        {
            //Debug.LogError("Successful attack");
            float Damage = Target.GetComponent<UnitCore>().Mounted == true ?
                CurrUnitCount * (AttackAgainstMounted + Random.Range(-0.1f, 0.1f) * AttackAgainstMounted) :
                CurrUnitCount * (AttackAgainstAfoot + Random.Range(-0.1f, 0.1f) * AttackAgainstAfoot);
            Target.GetComponent<UnitCore>().TakeDamage(this.gameObject, Damage, PenetrationPoints, false);
        }
    }

    public void TakeDamage(GameObject Attacker, float RawDamage, int PenetrationPoints, bool Retaliation)
    {
        float FinalDamage = RawDamage * (1 - (DefensePoints - PenetrationPoints) * 0.05f);
        if(PlayerUnit == false)
        {
            GameObject.Find("TurnManager").GetComponent<ScoreAndExperience>().AddScore(Mathf.RoundToInt(FinalDamage));
        }
        CurrTotalHealth -= Mathf.RoundToInt(FinalDamage);
        CurrUnitCount = Mathf.CeilToInt(CurrTotalHealth / UnitHealth);
        CheckDeath();
        if(!Retaliation)
        {
            int RetaliationRoll = Random.Range(1, 101);
            int BonusChanceToHit = 0;
            if (SelfTile.GetComponent<TileStats>().Height > Attacker.transform.parent.GetComponent<TileStats>().Height)
            {
                BonusChanceToHit = 5;
            }
            else if (SelfTile.GetComponent<TileStats>().Height < Attacker.transform.parent.GetComponent<TileStats>().Height)
            {
                BonusChanceToHit = -5;
            }
            if (RetaliationRoll < (ChanceToRetaliate+BonusChanceToHit))
            {
                float Damage = Attacker.GetComponent<UnitCore>().Mounted == true ? 
                    CurrUnitCount * (AttackAgainstMounted + Random.Range(-0.1f, 0.1f) * AttackAgainstMounted) : 
                    CurrUnitCount * (AttackAgainstAfoot + Random.Range(-0.1f, 0.1f) * AttackAgainstAfoot);
                Attacker.GetComponent<UnitCore>().TakeDamage(this.gameObject, Damage, PenetrationPoints, true);
            }
        }
    }
    public void WasteRemainingAp()
    {
        CurrentActionPoints = 0;
    }
    public void Resupply()
    {
        WasteRemainingAp();
        CurrSupply = MaxSupply;
        CurrTotalHealth += Random.Range(3, 7) * UnitHealth;
        CurrUnitCount = Mathf.CeilToInt(CurrTotalHealth / UnitHealth);
    }
    private void CheckDeath()
    {
        if(CurrUnitCount <= 0)
        {
            if(PlayerUnit == true)
            {
                GameObject.Find("SelectorObj").GetComponent<PlayerUnitSpawner>().RemoveUnit(this.gameObject);
            }
            else
            {
                GameObject.Find("EnemyManager").GetComponent<EnemySpawner>().RemoveUnit(this.gameObject);
            }
            Destroy(gameObject);
        }
    }

    public void RefreshAP()
    {
        CurrentActionPoints = MaxActionPoints;
    }

    public int GiveHitChanceAgainst(GameObject Target)
    {
        int BonusChanceToHit = 0;
        if (SelfTile.GetComponent<TileStats>().Height > Target.transform.parent.GetComponent<TileStats>().Height)
        {
            BonusChanceToHit = 5;
        }
        else if (SelfTile.GetComponent<TileStats>().Height < Target.transform.parent.GetComponent<TileStats>().Height)
        {
            BonusChanceToHit = -5;
        }
        return ChanceToHit + BonusChanceToHit;
    }

    public string GiveSuccessfulAttackMinMaxDamage(GameObject Target)
    {
        float MaxDamage = Target.GetComponent<UnitCore>().Mounted == true ? CurrUnitCount * (AttackAgainstMounted + 0.1f * AttackAgainstMounted) : CurrUnitCount * (AttackAgainstAfoot + 0.1f * AttackAgainstAfoot);
        float MinDamage = Target.GetComponent<UnitCore>().Mounted == true ? CurrUnitCount * (AttackAgainstMounted + (-0.1f * AttackAgainstMounted)) : CurrUnitCount * (AttackAgainstAfoot + (-0.1f * AttackAgainstAfoot));
        int MaxDamageToUnitsKilled = Mathf.FloorToInt((MaxDamage * (1 - (Target.GetComponent<UnitCore>().DefensePoints - PenetrationPoints) * 0.05f)) / Target.GetComponent<UnitCore>().UnitHealth);
        int MinDamageToUnitsKilled = Mathf.FloorToInt((MinDamage * (1 - (Target.GetComponent<UnitCore>().DefensePoints - PenetrationPoints) * 0.05f)) / Target.GetComponent<UnitCore>().UnitHealth);
        return "" + MinDamageToUnitsKilled + "/" + MaxDamageToUnitsKilled;
    }

    public int GiveRetaliationChanceAgainst(GameObject Attacker)
    {
        int BonusChanceToHit = 0;
        if (SelfTile.GetComponent<TileStats>().Height > Attacker.transform.parent.GetComponent<TileStats>().Height)
        {
            BonusChanceToHit = 5;
        }
        else if (SelfTile.GetComponent<TileStats>().Height < Attacker.transform.parent.GetComponent<TileStats>().Height)
        {
            BonusChanceToHit = -5;
        }
        return ChanceToRetaliate + BonusChanceToHit;
    }

    public void UpgradeUnitTo1() // Mounted attacker
    {
        UnitType = 1;
        Mounted = true;
        MaxActionPoints = 15;
        MaxSupply = 9;
        UnitHealth = 105;
        MaxUnitCount = 14;
        AttackAPCost = 5;
        ChanceToHit = 90;
        ChanceToRetaliate = 45;
        DefensePoints = 3;
        AttackAgainstAfoot = 36;
        AttackAgainstMounted = 36;
        PenetrationPoints = 2;
    }

    public void UpgradeUnitTo2() // afoot defender
    {
        UnitType = 2;
        Mounted = false;
        MaxActionPoints = 15;
        MaxSupply = 12;
        UnitHealth = 110;
        MaxUnitCount = 14;
        AttackAPCost = 5;
        ChanceToHit = 75;
        ChanceToRetaliate = 80;
        DefensePoints = 7;
        AttackAgainstAfoot = 30;
        AttackAgainstMounted = 28;
        PenetrationPoints = 0;
    }

    private string GenerateRandomName()
    {
        string[] Adjectives = { "Raging", "Flying", "Lucky", "Watchful", "Bloody", "Golden", "Screaming",
            "Old", "Ancient", "Hell's", "Thundering", "Bronze", "Invisible", "Unseen",
            "Lost", "Thirsty", "Hungry", "Misty", "Hallowed" };
        string[] Subject = { "Hussars", "Marauders", "Angels", "Hawks", "Guards", "Ironsides", "Snakes",
            "Phantoms", "Scouts", "Devils", "Pathfinders", "Waterseekers", "Sandwalkers", "Shadows",
            "Wanderers", "Exiles", "Dragoons", "Knots", "Hounds", "Celestials" };
        // format X or XX number + adjective + unit type
        string name = "";
        int rand = Random.Range(1, 100);
        switch (rand)
        {
            case 1 or 21 or 31 or 41 or 51 or 61 or 71 or 81 or 91: name += rand + "st"; break;
            case 2 or 22 or 32 or 42 or 52 or 62 or 72 or 82 or 92: name += rand + "nd"; break;
            case 3 or 23 or 33 or 43 or 53 or 63 or 73 or 83 or 93: name += rand + "rd"; break;
            default: name += rand + "th"; break;
        }
        name += " " + Adjectives[Random.Range(0, Adjectives.Length)] + " " + Subject[Random.Range(0, Subject.Length)];
        return name;
    }
    private string GenerateRandomEnemyName()
    {
        string[] Aspect = { "Ignis", "Ordo", "Perditio", "Gelum", "Lux", "Motus", "Potentia", "Vacuos", "Vitreus", "Fames", "Iter", "Limus", "Metallum", "Volatus", "Auram", "Vitium", "Sensus", "Lucrum", "Messis", "Perfodio", "Meto", "Pannus", "Telum", "Terminus" };
        string[] Subject = { "Solitudinem", "Mons", "Vallis", "Ventus", "Harenae", "Solis", "Luna", "Maris", "Colles", "Noctis", "Insulam", "Silva", "Antrum" };

        //format Legion (number in roman) + Subject
        string name = "";
        name += "Legion ";
        int rand = Random.Range(1, 30);
        if (rand > 9)
        {
            name += "X";
        }else if(rand > 19)
        {
            name += "XX";
        }
        switch (rand % 10)
        {
            case 1: name += "I"; break;
            case 2: name += "II"; break;
            case 3: name += "III"; break;
            case 4: name += "IV"; break;
            case 5: name += "V"; break;
            case 6: name += "VI"; break;
            case 7: name += "VII"; break;
            case 8: name += "VIII"; break;
            case 9: name += "IX"; break;
            default: break;
        }
        name += " " + Aspect[Random.Range(0, Aspect.Length - 1)];
        rand = Random.Range(0, 3);
        switch (rand)
        {
            case 0: name += " o'"; break;
            case 1: name += " e'"; break;
            case 2: name += " a'"; break;
            default:break;
        }
        name += " " + Subject[Random.Range(0, Subject.Length - 1)];
        return name;
    }
}

/*

public static class UnitAttackCalculator
{
    public static int DefensiveBonus = 2;
}

public class DisplayWithoutEdit : PropertyAttribute
{
    public DisplayWithoutEdit()
    {

    }
}

[CustomPropertyDrawer(typeof(DisplayWithoutEdit))]
public class DisplayWithoutEditDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        switch (property.propertyType)
        {
            case SerializedPropertyType.Boolean:
                EditorGUI.LabelField(position, label, new GUIContent(property.boolValue.ToString()));
                break;
            case SerializedPropertyType.Enum:
                EditorGUI.LabelField(position, label, new GUIContent(property.enumDisplayNames[property.enumValueIndex]));
                break;
            case SerializedPropertyType.Float:
                EditorGUI.LabelField(position, label, new GUIContent(property.floatValue.ToString()));
                break;
            case SerializedPropertyType.Integer:
                EditorGUI.LabelField(position, label, new GUIContent(property.intValue.ToString()));
                break;
            case SerializedPropertyType.String:
                EditorGUI.LabelField(position, label, new GUIContent(property.stringValue));
                break;
            default: break;
        }
    }
}

*/