using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField]
    private GameObject BaseEnemyUnit;
    public List<GameObject> AllEnemies = new List<GameObject>();
    public int InitialNumbersToSpawn;

    public void SpawnInitialEnemies()
    {
        GameObject MapGenObj = MapGenScriptStatics.MapGenObject;
        while (InitialNumbersToSpawn > 0)
        {
            GameObject PotentialTile = MapGenObj.transform.GetChild(Random.Range(0, MapGenObj.transform.childCount - 1)).gameObject;
            if(PotentialTile.transform.childCount != 2) // not occupied by units
            {
                GameObject tempUnit = Instantiate(BaseEnemyUnit, PotentialTile.transform);
                AllEnemies.Add(tempUnit);
                InitialNumbersToSpawn -= 1;
            }
        }
    }

    public void RemoveUnit(GameObject x)
    {
        AllEnemies.Remove(x);
        if(AllEnemies.Count == 0)
        {
            GameObject.Find("Canvas").transform.Find("WinLossScreen").GetComponent<WinLoss>().TriggerVictory();
        }
    }
}
