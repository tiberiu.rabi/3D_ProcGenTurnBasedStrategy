using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileTypeDecider : MonoBehaviour
{
    [SerializeField]
    private Vector3[] DecorPositions;
    [SerializeField]
    private GameObject Empty;

    public List<int> PossibleHeightOptions = new List<int>();
    public List<int> PossibleBiomeOptions = new List<int>();

    public float HeightStepDist;

    public int ChosenHeight;
    public int ChosenBiome;
    public bool isSettlement;

    private int[] NeighbourTileHeights = new int[6];

    public void RemoveHeightOpt(int Height)
    {
        int MaxPossibleHeight = Mathf.Max(PossibleHeightOptions.ToArray());
        for(int x1 = 0; x1 <= MaxPossibleHeight; x1++)
        {
            if (x1 < Height - 1 || x1 > Height + 1)
            {
                PossibleHeightOptions.Remove(x1);
            }
        }
    }

    public void RemoveBiomeOpt(int Biome)
    {
        int MaxPossBiome = Mathf.Max(PossibleBiomeOptions.ToArray());
        for (int x1 = 0; x1 <= MaxPossBiome; x1++)
        {
            if (x1 < Biome - 1 || x1 > Biome + 1)
            {
                PossibleBiomeOptions.Remove(x1);
            }
        }
    }

    private void TellNearby(bool Height, bool Biome)
    {
        for (int x1 = 0; x1 < 6; x1++)
        {
            Vector2 ThisPos2D = new Vector2(transform.position.x, transform.position.z) + MapGenScriptStatics.HexRelativePositions[x1];
            for(int ChildIndex = 0; ChildIndex < transform.parent.childCount; ChildIndex++)
            {
                Vector2 ChildPos = new Vector2(transform.parent.transform.GetChild(ChildIndex).position.x, transform.parent.transform.GetChild(ChildIndex).position.z);
                if (Vector2.Distance(ThisPos2D, ChildPos) < 0.2f)
                {
                    if(Height == true)
                    {
                        transform.parent.transform.GetChild(ChildIndex).transform.GetComponent<TileTypeDecider>().RemoveHeightOpt(ChosenHeight);
                    }
                    if(Biome == true)
                    {
                        transform.parent.transform.GetChild(ChildIndex).transform.GetComponent<TileTypeDecider>().RemoveBiomeOpt(ChosenBiome);
                    }
                }
            }
        }
    }

    public void ApplyBiome()
    {
        Material selfMat = transform.GetComponent<Renderer>().material;
        selfMat.color = BiomeColors.BiomeColorsList.ToArray()[ChosenBiome - 1];
        if(isSettlement == true)
        {
            selfMat.color = BiomeColors.SettlementColor;
        }
        transform.GetComponent<Renderer>().material = selfMat;
        GameObject BiomeObjectsHolder = Instantiate(Empty, this.transform);
        BiomeObjectsHolder.transform.name = "BiomeObjectHolder";
        if (BiomeDecorations.BiomeDecors[ChosenBiome - 1].Count > 0)
        {
            foreach (Vector3 x in DecorPositions)
            {
                int Rand = Random.Range(1, 101);
                if(Rand> 10) // some spots don't get decor
                {
                    if(isSettlement == true)
                    {
                        Rand = Random.Range(1, 101);
                        GameObject tempDecor;
                        if (Rand < 75)
                        {
                            tempDecor = Instantiate(BiomeDecorations.BiomeDecors[ChosenBiome - 1].ToArray()[Random.Range(0, BiomeDecorations.BiomeDecors[ChosenBiome - 1].Count)],BiomeObjectsHolder.transform.position + x, Quaternion.identity);
                        }
                        else
                        {
                            tempDecor = Instantiate(BiomeDecorations.BiomeDecors[7].ToArray()[Random.Range(0, BiomeDecorations.BiomeDecors[7].Count)], BiomeObjectsHolder.transform.position + x, Quaternion.identity);
                        }
                        tempDecor.transform.parent = BiomeObjectsHolder.transform;
                        tempDecor.transform.position += new Vector3(Random.Range(-0.03f, 0.03f), 0, Random.Range(-0.03f, 0.03f));
                        tempDecor.transform.localScale *= (0.20f + Random.Range(0, 0.05f));
                        tempDecor.transform.Rotate(new Vector3(0, 1, 0), Random.Range(0, 360));
                    }
                    else
                    {
                        GameObject tempDecor = Instantiate(BiomeDecorations.BiomeDecors[ChosenBiome - 1].ToArray()[Random.Range(0, BiomeDecorations.BiomeDecors[ChosenBiome - 1].Count)], BiomeObjectsHolder.transform.position + x, Quaternion.identity);
                        tempDecor.transform.parent = BiomeObjectsHolder.transform;
                        tempDecor.transform.position += new Vector3(Random.Range(-0.03f, 0.03f), 0, Random.Range(-0.03f, 0.03f));
                        tempDecor.transform.localScale *= (0.20f + Random.Range(0, 0.05f));
                        tempDecor.transform.Rotate(new Vector3(0, 1, 0), Random.Range(0, 360));
                    }
                }
            }
        }
    }

    public void DecideBiome()
    {
        ChosenBiome = PossibleBiomeOptions.ToArray()[Random.Range(0, PossibleBiomeOptions.Count)];
        TellNearby(false, true);
        int SettlementRand = Random.Range(0, 100);
        isSettlement = SettlementRand < 4 ? true : false;
        ApplyBiome();
        this.transform.GetComponent<TileStats>().Biome = ChosenBiome;
        this.transform.GetComponent<TileStats>().isSettlement = isSettlement;
        
    }
    public void DecideBiomeAskAround()
    {
        for (int x1 = 0; x1 < 6; x1++)
        {
            Vector2 ThisPos2D = new Vector2(transform.position.x, transform.position.z) + MapGenScriptStatics.HexRelativePositions[x1];
            for (int ChildIndex = 0; ChildIndex < transform.parent.childCount; ChildIndex++)
            {
                Vector2 ChildPos = new Vector2(transform.parent.transform.GetChild(ChildIndex).position.x, transform.parent.transform.GetChild(ChildIndex).position.z);
                if (Vector2.Distance(ThisPos2D, ChildPos) < 0.2f)
                {
                    if (transform.parent.transform.GetChild(ChildIndex).transform.GetComponent<TileTypeDecider>().ChosenHeight != 0)
                    {
                        RemoveBiomeOpt(transform.parent.transform.GetChild(ChildIndex).transform.GetComponent<TileTypeDecider>().ChosenBiome);
                    }
                }
            }
        }
        ChosenBiome = PossibleBiomeOptions.ToArray()[Random.Range(0, PossibleBiomeOptions.Count)];
        TellNearby(false, true);
        int SettlementRand = Random.Range(0, 100);
        isSettlement = SettlementRand < 4 ? true : false;
        ApplyBiome();
        this.transform.GetComponent<TileStats>().Biome = ChosenBiome;
        this.transform.GetComponent<TileStats>().isSettlement = isSettlement;
    }

    public void DecideHeight()
    {
        ChosenHeight = PossibleHeightOptions.ToArray()[Random.Range(0, PossibleHeightOptions.Count)];
        Vector3 pos = transform.position;
        pos.y = HeightStepDist * ChosenHeight - 0.2f;
        transform.position = pos;
        TellNearby(true, false);
        this.transform.GetComponent<TileStats>().Height = ChosenHeight;
    }

    public void DecideHeightAskAround()
    {
        for (int x1 = 0; x1 < 6; x1++)
        {
            Vector2 ThisPos2D = new Vector2(transform.position.x, transform.position.z) + MapGenScriptStatics.HexRelativePositions[x1];
            for (int ChildIndex = 0; ChildIndex < transform.parent.childCount; ChildIndex++)
            {
                Vector2 ChildPos = new Vector2(transform.parent.transform.GetChild(ChildIndex).position.x, transform.parent.transform.GetChild(ChildIndex).position.z);
                if (Vector2.Distance(ThisPos2D, ChildPos) < 0.2f)
                {
                    if(transform.parent.transform.GetChild(ChildIndex).transform.GetComponent<TileTypeDecider>().ChosenHeight != 0)
                    {
                        RemoveHeightOpt(transform.parent.transform.GetChild(ChildIndex).transform.GetComponent<TileTypeDecider>().ChosenHeight);
                    }
                }
            }
        }
        ChosenHeight = PossibleHeightOptions.ToArray()[Random.Range(0, PossibleHeightOptions.Count)];
        Vector3 pos = transform.position;
        pos.y = HeightStepDist * ChosenHeight - 0.2f;
        transform.position = pos;
        TellNearby(true, false);
        this.transform.GetComponent<TileStats>().Height = ChosenHeight;
    }

    public void UpdateMesh()
    {
        for (int x1 = 0; x1 < 6; x1++)
        {
            bool Found = false;
            Vector2 ThisPos2D = new Vector2(transform.position.x, transform.position.z) + MapGenScriptStatics.HexRelativePositions[x1];
            for (int ChildIndex = 0; ChildIndex < transform.parent.childCount; ChildIndex++)
            {
                Vector2 ChildPos = new Vector2(transform.parent.transform.GetChild(ChildIndex).position.x, transform.parent.transform.GetChild(ChildIndex).position.z);
                if (Vector2.Distance(ThisPos2D, ChildPos) < 0.2f)
                {
                    Found = true;
                    NeighbourTileHeights[x1] = transform.parent.transform.GetChild(ChildIndex).transform.GetComponent<TileTypeDecider>().ChosenHeight;
                }
            }
            if(Found == false)
            {
                NeighbourTileHeights[x1] = 0;
            }
        }
        this.transform.GetComponent<TileMeshGenerator>().UpdateSelfMesh(NeighbourTileHeights, ChosenHeight, HeightStepDist);
    }

    public void UpdateMeshAndAround()
    {
        for (int x1 = 0; x1 < 6; x1++)
        {
            bool Found = false;
            Vector2 ThisPos2D = new Vector2(transform.position.x, transform.position.z) + MapGenScriptStatics.HexRelativePositions[x1];
            for (int ChildIndex = 0; ChildIndex < transform.parent.childCount; ChildIndex++)
            {
                Vector2 ChildPos = new Vector2(transform.parent.transform.GetChild(ChildIndex).position.x, transform.parent.transform.GetChild(ChildIndex).position.z);
                if (Vector2.Distance(ThisPos2D, ChildPos) < 0.2f)
                {
                    Found = true;
                    transform.parent.transform.GetChild(ChildIndex).transform.GetComponent<TileTypeDecider>().UpdateMesh();
                    NeighbourTileHeights[x1] = transform.parent.transform.GetChild(ChildIndex).transform.GetComponent<TileTypeDecider>().ChosenHeight;
                }
            }
            if (Found == false)
            {
                NeighbourTileHeights[x1] = 0;
            }
        }
        this.transform.GetComponent<TileMeshGenerator>().UpdateSelfMesh(NeighbourTileHeights, ChosenHeight, HeightStepDist);
    }
}
