using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CornerUI : MonoBehaviour
{
    void Start()
    {
        ToggleSettingsWindow();
        LoadMusicVolumeSetting();
        LoadSoundsVolumeSetting();
        LoadEffectsVolumeSetting();
        LoadAnimationSpeedSetting();
    }

    public void SaveMusicVolumeSetting()
    {
        StaticSettings.MusicVolume = transform.Find("MusicVSetting").GetComponent<Scrollbar>().value;
        transform.Find("MusicVSetting").transform.GetChild(1).GetComponent<TMP_Text>().text = Mathf.Round(StaticSettings.MusicVolume * 100) + "%";
        PlayerPrefs.SetFloat("MusicVolume", StaticSettings.MusicVolume);
        PlayerPrefs.Save();
    }

    private void LoadMusicVolumeSetting()
    {
        StaticSettings.MusicVolume = PlayerPrefs.GetFloat("MusicVolume");
        transform.Find("MusicVSetting").transform.GetChild(1).GetComponent<TMP_Text>().text = Mathf.Round(StaticSettings.MusicVolume * 100) + "%";
        transform.Find("MusicVSetting").GetComponent<Scrollbar>().value = StaticSettings.MusicVolume;
    }

    public void SaveSoundsVolumeSetting()
    {
        StaticSettings.SoundsVolume = transform.Find("SoundsVSetting").GetComponent<Scrollbar>().value;
        transform.Find("SoundsVSetting").transform.GetChild(1).GetComponent<TMP_Text>().text = Mathf.Round(StaticSettings.SoundsVolume * 100) + "%";
        PlayerPrefs.SetFloat("SoundsVolume", StaticSettings.SoundsVolume);
        PlayerPrefs.Save();
    }

    private void LoadSoundsVolumeSetting()
    {
        StaticSettings.SoundsVolume = PlayerPrefs.GetFloat("SoundsVolume");
        transform.Find("SoundsVSetting").transform.GetChild(1).GetComponent<TMP_Text>().text = Mathf.Round(StaticSettings.SoundsVolume * 100) + "%";
        transform.Find("SoundsVSetting").GetComponent<Scrollbar>().value = StaticSettings.SoundsVolume;
    }

    public void SaveEffectsVolumeSetting()
    {
        StaticSettings.SoundsVolume = transform.Find("EffectsVSetting").GetComponent<Scrollbar>().value;
        transform.Find("EffectsVSetting").transform.GetChild(1).GetComponent<TMP_Text>().text = Mathf.Round(StaticSettings.SoundsVolume * 100) + "%";
        PlayerPrefs.SetFloat("EffectsVolume", StaticSettings.SoundsVolume);
        PlayerPrefs.Save();
    }

    private void LoadEffectsVolumeSetting()
    {
        StaticSettings.SoundsVolume = PlayerPrefs.GetFloat("EffectsVolume");
        transform.Find("EffectsVSetting").transform.GetChild(1).GetComponent<TMP_Text>().text = Mathf.Round(StaticSettings.SoundsVolume * 100) + "%";
        transform.Find("EffectsVSetting").GetComponent<Scrollbar>().value = StaticSettings.SoundsVolume;
    }

    public void SaveAnimationSpeedSetting()
    {
        switch (transform.Find("AnimSpeedSetting").GetComponent<Scrollbar>().value)
        {
            case <= 0.125f: StaticSettings.AnimSpeedMultiplier = 0.75f; break;
            case > 0.125f and < 0.375f: StaticSettings.AnimSpeedMultiplier = 1f; break;
            case >= 0.375f and <= 0.625f: StaticSettings.AnimSpeedMultiplier = 1.25f; break;
            case > 0.625f and < 0.875f: StaticSettings.AnimSpeedMultiplier = 1.5f; break;
            case >= 0.875f: StaticSettings.AnimSpeedMultiplier = 2f; break;
            default: StaticSettings.AnimSpeedMultiplier = 1f; break;
        }
        transform.Find("AnimSpeedSetting").transform.GetChild(1).transform.GetComponent<TMP_Text>().text = "x" + StaticSettings.AnimSpeedMultiplier.ToString("0.##");
        PlayerPrefs.SetFloat("AnimSpeedSetting", transform.Find("AnimSpeedSetting").GetComponent<Scrollbar>().value);
    }

    private void LoadAnimationSpeedSetting()
    {
        transform.Find("AnimSpeedSetting").GetComponent<Scrollbar>().value = PlayerPrefs.GetFloat("AnimSpeedSetting");
        switch (transform.Find("AnimSpeedSetting").GetComponent<Scrollbar>().value)
        {
            case <= 0.125f: StaticSettings.AnimSpeedMultiplier = 0.75f; break;
            case > 0.125f and < 0.375f: StaticSettings.AnimSpeedMultiplier = 1f; break;
            case >= 0.375f and <= 0.625f: StaticSettings.AnimSpeedMultiplier = 1.25f; break;
            case > 0.625f and < 0.875f: StaticSettings.AnimSpeedMultiplier = 1.5f; break;
            case >= 0.875f: StaticSettings.AnimSpeedMultiplier = 2f; break;
            default: StaticSettings.AnimSpeedMultiplier = 1f; break;
        }
        transform.Find("AnimSpeedSetting").transform.GetChild(1).transform.GetComponent<TMP_Text>().text = "x" + StaticSettings.AnimSpeedMultiplier.ToString("0.##");
    }

    public void ToggleSettingsWindow()
    {
        Vector3 Pos = transform.GetComponent<RectTransform>().anchoredPosition;
        Pos.y *= -1;
        transform.GetComponent<RectTransform>().anchoredPosition = Pos;
        string direction = Pos.y < 0 ? "/\\" : "\\/";
        transform.Find("ToggleSettings").transform.GetChild(0).GetComponent<TMP_Text>().text = direction;
    }

    public void QuitToMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void QuitToDesktop()
    {
        Application.Quit();
    }
}
