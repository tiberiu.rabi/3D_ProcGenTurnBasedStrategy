using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    private float CameraRotationQESpeed = 300f;

    private float LerpValue = 1;
    private float LerpStepValue = 0.04f;
    private float MaxDistanceY = 12f;
    private float MinDistanceY = 2f;
    private float MaxDistAngle = 88f;
    private float MinDistAngle = 48f;
    private float CameraMoveSpeedMultiplier = 0.2f;

    void Update()
    {
        Vector3 DirectionRaw = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")); //almost works
        Vector3 DirectionRotated = Quaternion.AngleAxis(Camera.main.transform.rotation.eulerAngles.y, Vector3.up) * DirectionRaw;
        transform.Translate(DirectionRotated * CameraMoveSpeedMultiplier, Space.World);
        if(Input.GetAxis("Mouse ScrollWheel") > 0) // zoom in
        {
            LerpValue = LerpValue > 0 ? LerpValue - LerpStepValue : 0f;

            Vector3 CurrentPos = Camera.main.transform.position;
            CurrentPos.y = Mathf.Lerp(MinDistanceY, MaxDistanceY, LerpValue);
            Camera.main.transform.position = CurrentPos;

            Vector3 CurrentRot = Camera.main.transform.rotation.eulerAngles;
            CurrentRot.x = Mathf.Lerp(MinDistAngle, MaxDistAngle, LerpValue);
            Camera.main.transform.rotation = Quaternion.Euler(CurrentRot);
        }
        else if(Input.GetAxis("Mouse ScrollWheel") < 0) // zoom out
        {
            LerpValue = LerpValue < 1f ? LerpValue + LerpStepValue : 1f;

            Vector3 CurrentPos = Camera.main.transform.position;
            CurrentPos.y = Mathf.Lerp(MinDistanceY, MaxDistanceY, LerpValue);
            Camera.main.transform.position = CurrentPos;

            Vector3 CurrentRot = Camera.main.transform.rotation.eulerAngles;
            CurrentRot.x = Mathf.Lerp(MinDistAngle, MaxDistAngle, LerpValue);
            Camera.main.transform.rotation = Quaternion.Euler(CurrentRot);
        }

        if (Input.GetMouseButton(2)) //rotate around Y
        {
            if(Mathf.Abs(Input.GetAxis("Mouse X")) > 0.1f)
            {
                Camera.main.transform.Rotate(new Vector3(0, 1, 0), Input.GetAxis("Mouse X") * CameraRotationQESpeed * Time.deltaTime, Space.World);
            }
        }
    }
}
