using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class WinLoss : MonoBehaviour
{
    private Image SelfImageComp;
    private TMP_Text WinLossText;

    void Start()
    {
        SelfImageComp = transform.GetComponent<Image>();
        WinLossText = transform.GetChild(0).GetComponent<TMP_Text>();
        for(int child = 0; child < transform.childCount; child++)
        {
            transform.GetChild(child).gameObject.SetActive(false);
        }
        SelfImageComp.enabled = false;
    }

    public void TriggerVictory()
    {
        WinLossText.text = "You Won!";
        for (int child = 0; child < transform.childCount; child++)
        {
            transform.GetChild(child).gameObject.SetActive(true);
        }
        SelfImageComp.enabled = true;
    }

    public void TriggerLoss()
    {
        WinLossText.text = "You Lost!";
        for (int child = 0; child < transform.childCount; child++)
        {
            transform.GetChild(child).gameObject.SetActive(true);
        }
        SelfImageComp.enabled = true;
    }
}
