using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyActionTaker : MonoBehaviour
{
    private List<GameObject> EnemyUnits = new List<GameObject>();
    private EnemySpawner SpawnerScript;

    public void Start()
    {
        SpawnerScript = transform.GetComponent<EnemySpawner>();
    }

    public void RefreshUnitsAP()
    {
        EnemyUnits = SpawnerScript.AllEnemies;
        foreach(GameObject x in EnemyUnits)
        {
            if(x == null)
            {
                SpawnerScript.RemoveUnit(x);
                break;
            }
        }
        EnemyUnits = SpawnerScript.AllEnemies;
        foreach (GameObject x in EnemyUnits)
        {
            x.GetComponent<UnitCore>().RefreshAP();
        }
    }

    public void TakeActions()
    {
        GameObject[] PlayerUnits = GameObject.Find("SelectorObj").GetComponent<PlayerUnitSpawner>().AllPlayerUnits.ToArray();
        EnemyUnits = SpawnerScript.AllEnemies;
        foreach(GameObject x in EnemyUnits) // control each enemy unit
        {
            UnitCore xCore = x.GetComponent<UnitCore>();
            GameObject ClosestEnemy = PlayerUnits[0];
            foreach(GameObject pUnit1 in PlayerUnits)
            {
                if (pUnit1!=null && ClosestEnemy!=null && Vector3.Distance(x.transform.position, pUnit1.transform.position) < Vector3.Distance(x.transform.position, ClosestEnemy.transform.position))
                {
                    ClosestEnemy = pUnit1;
                }
            }
            if (ClosestEnemy!=null && xCore.CheckIfNeighbor(ClosestEnemy) == true)
            {
                while(xCore.CurrentActionPoints > xCore.AttackAPCost)
                {
                    xCore.Attack(ClosestEnemy);
                }
                xCore.WasteRemainingAp();
            }
            else // not next to player
            {
                if (ClosestEnemy!=null && xCore.CurrTotalHealth > ((xCore.MaxUnitCount / 2) * xCore.UnitHealth)) // if over half hp be agressive
                {
                    float Dist = Vector3.Distance(ClosestEnemy.transform.position, xCore.transform.position);
                    foreach (Vector2 relativDir in MapGenScriptStatics.HexRelativePositions)
                    {
                        float tempDist = Vector3.Distance(ClosestEnemy.transform.position, xCore.transform.position + new Vector3(relativDir.x, 0, relativDir.y));
                        if(tempDist < Dist)
                        {
                            Vector3 actualDir = xCore.transform.position + new Vector3(relativDir.x, 0, relativDir.y);
                            xCore.MoveToTile(GameObject.Find("MapGenerator").GetComponent<MapFindTile>().FindChildByPosNotNull(actualDir));
                        }
                    }
                    xCore.WasteRemainingAp();
                }
                else // if lower than half hp
                {
                    if(xCore.CurrentActionPoints == xCore.MaxActionPoints)
                    {
                        xCore.Resupply();
                    }
                    else
                    {
                        xCore.WasteRemainingAp();
                    }
                }
            }
        }
        //do stuff
        GameObject.Find("TurnManager").GetComponent<TurnManager>().EndEnemyTurn();
    }
}
