using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerUnitSpawner : MonoBehaviour
{
    public GameObject BasePlayerUnit;
    public int InitialUnitCount;
    public List<GameObject> AllPlayerUnits = new List<GameObject>();
    private int NumberOfAllowedTries = 25;
    public void SpawnInitialPlayerUnits()
    {
        GameObject MapGenObj = MapGenScriptStatics.MapGenObject;
        while (InitialUnitCount > 0 && NumberOfAllowedTries>0)
        {
            NumberOfAllowedTries -= 1;
            GameObject PotentialTile = MapGenObj.GetComponent<MapFindTile>().ReturnRandomSettlementTile();
            if (PotentialTile.transform.childCount != 2) // not occupied by units
            {
                GameObject tempUnit = Instantiate(BasePlayerUnit, PotentialTile.transform);
                AllPlayerUnits.Add(tempUnit);
                InitialUnitCount -= 1;
            }
        }
    }

    public void RemoveUnit(GameObject x)
    {
        AllPlayerUnits.Remove(x);
        if(AllPlayerUnits.Count == 0)
        {
            GameObject.Find("Canvas").transform.Find("WinLossScreen").GetComponent<WinLoss>().TriggerLoss();
        }
    }
}
