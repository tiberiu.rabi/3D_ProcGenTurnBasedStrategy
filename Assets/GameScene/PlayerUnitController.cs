using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerUnitController : MonoBehaviour
{
    private List<GameObject> PlayerUnits = new List<GameObject>();
    private PlayerUnitSpawner SpawnerScrit;

    private void Start()
    {
        SpawnerScrit = transform.GetComponent<PlayerUnitSpawner>();
    }

    public void RefreshUnitsAP()
    {
        PlayerUnits = SpawnerScrit.AllPlayerUnits;
        foreach (GameObject x in PlayerUnits)
        {
            if (x != null)
            {
                x.GetComponent<UnitCore>().RefreshAP();
            }
            else
            {
                SpawnerScrit.RemoveUnit(x);
            }
        }
    }
}
