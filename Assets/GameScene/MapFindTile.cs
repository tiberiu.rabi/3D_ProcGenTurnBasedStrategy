using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapFindTile : MonoBehaviour
{
    private float MinPrecision = 0.5f;

    public List<GameObject> SettlementTiles = new List<GameObject>();

    public GameObject GetChildByPosition(Vector3 Pos)
    {
        GameObject FoundChild = null;
        for(int x1 = 0; x1 < transform.childCount; x1++)
        {
            if (Vector3.Distance(transform.GetChild(x1).transform.position, Pos) < MinPrecision)
            {
                FoundChild = transform.GetChild(x1).gameObject;
                break;
            }
        }
        return FoundChild;
    }

    public GameObject FindChildByPosNotNull(Vector3 pos)
    {
        GameObject FoundChild = transform.GetChild(0).gameObject;
        float minDistance = Vector3.Distance(pos, FoundChild.transform.position);
        for(int x1 = 0; x1 < transform.childCount; x1++)
        {
            if (Vector3.Distance(transform.GetChild(x1).transform.position, pos) < minDistance)
            {
                FoundChild = transform.GetChild(x1).gameObject;
                minDistance = Vector3.Distance(transform.GetChild(x1).transform.position, pos);
            }
        }
        return FoundChild;
    }

    public GameObject ReturnRandomSettlementTile()
    {
        return SettlementTiles.ToArray()[Random.Range(0, SettlementTiles.Count - 1)];
    }

    public void FindAllSettlementTiles()
    {
        for(int x1 = 0; x1 < transform.childCount; x1++)
        {
            GameObject temp = transform.GetChild(x1).gameObject;
            if (temp.GetComponent<TileStats>().isSettlement == true && !SettlementTiles.Contains(temp))
            {
                SettlementTiles.Add(temp);
            }
        }
    }
}
