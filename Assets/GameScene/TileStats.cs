using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TileStats : MonoBehaviour
{
    public int Biome;
    public int Height;
    public bool isSettlement;

    //GrassLands AcaciaForest EasyWeastelands CactusForest BoneForest ExtremeDesert DesertWasteland
    private int[] BiomeMoveCostBase = {2, 2, 4, 5, 3, 5, 4 };
    private int[] BiomeMoveCostMounted = {2, 3, 3, 4, 2, 4, 3 };

    public int MoveIntoCost(bool Mounted, int FromHeight)
    {
        int CostToMoveInto = 0;
        if(Mounted == true)
        {
            CostToMoveInto += BiomeMoveCostMounted[Biome - 1];
        }
        else
        {
            CostToMoveInto += BiomeMoveCostBase[Biome - 1];
        }
        if(FromHeight > Height)
        {
            CostToMoveInto -= 1;
        }
        else if(FromHeight < Height)
        {
            CostToMoveInto += 1;
        }
        if(isSettlement == true)
        {
            CostToMoveInto += 1;
        }
        return CostToMoveInto;
    }

    public void ShowStats()
    {
        string Displaytext = "Biome: ";
        switch (Biome)
        {
            case 1: Displaytext += "Grasslands "; break;
            case 2: Displaytext += "Acacia forest "; break;
            case 3: Displaytext += "Light Wastelands "; break;
            case 4: Displaytext += "Cactus Forest "; break;
            case 5: Displaytext += "Bone Forest "; break;
            case 6: Displaytext += "Extreme Desert "; break;
            case 7: Displaytext += "Desert Wastelands "; break;
            default: Displaytext += "Something went wrong "; break;
        }
        if (isSettlement == true)
        {
            Displaytext += "Settlement ";
        }
        Displaytext += "- Height: " + Height;
        Displaytext += " - Mounted Move Cost: " + BiomeMoveCostMounted[Biome-1];
        Displaytext += " - Afoot Move Cost: " + BiomeMoveCostBase[Biome-1];
        CanvasLink.HexInfoBottomObj.GetComponent<TMP_Text>().text = Displaytext;
    }
}
