using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenScript : MonoBehaviour
{
    public List<Color> ColorsToAddToStatic;
    public Color SettlementColor;

    public List<GameObject> AllInstTiles = new List<GameObject>();
    public GameObject Tile;
    public int InitialMapSizeZ;
    public int InitialMapSizeX;
    
    [System.NonSerialized]
    public float HexRelativeZ = 2.1317f; //height
    [System.NonSerialized]
    public float HexRelativeX = 1.846f; //width

    private int IndexTop;
    private int IndexBottom;
    private int IndexRight;
    private int IndexLeft;

    private int CurrentIndexZ = 0;
    private int CurrentIndexX = 0;

    private int CurrentAction = 1; //1 gen, 2 Height, 3 Mesh, 4 biome
    private int CurrentActionIndex = 0;
    private bool ActionDone = false;

    public List<GameObject> BiomeGrasslandsDecor;
    public List<GameObject> BiomeAcaciaForestDecor;
    public List<GameObject> BiomeEasyWastelandDecor;
    public List<GameObject> BiomeCactusForestDecor;
    public List<GameObject> BiomeBoneForestDecor;
    public List<GameObject> BiomeExtremeDesertDecor;
    public List<GameObject> BiomeDesertWastelandDecor;
    public List<GameObject> SettlementDecor;


    void Start()
    {
        CanvasLink.Canvas = GameObject.Find("Canvas");
        CanvasLink.HexInfoBottomObj = GameObject.Find("Canvas").transform.Find("HexInfoBottom").gameObject;
        MapGenScriptStatics.MapGenObject = this.transform.gameObject;
        BiomeColors.BiomeColorsList = ColorsToAddToStatic;
        BiomeColors.SettlementColor = SettlementColor;
        IndexBottom = 0;
        IndexLeft = 0;
        IndexTop = InitialMapSizeZ-1;
        IndexRight = InitialMapSizeX-1;

        BiomeDecorations.BiomeDecors[0] = BiomeGrasslandsDecor;
        BiomeDecorations.BiomeDecors[1] = BiomeAcaciaForestDecor;
        BiomeDecorations.BiomeDecors[2] = BiomeEasyWastelandDecor;
        BiomeDecorations.BiomeDecors[3] = BiomeCactusForestDecor;
        BiomeDecorations.BiomeDecors[4] = BiomeBoneForestDecor;
        BiomeDecorations.BiomeDecors[5] = BiomeExtremeDesertDecor;
        BiomeDecorations.BiomeDecors[6] = BiomeDesertWastelandDecor;
        BiomeDecorations.BiomeDecors[7] = SettlementDecor;
    }

    // Update is called once per frame
    void Update()
    {
        
        if (Input.GetKeyDown(KeyCode.U))
        {
            StartCoroutine(GenTilesToRight());
        }
        if (Input.GetKeyDown(KeyCode.I))
        {
            StartCoroutine(GenTilesToTop());
        }
        if (Input.GetKeyDown(KeyCode.O))
        {
            StartCoroutine(GenTilesToLeft());
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            StartCoroutine(GenTilesToBottom());
        }
        


        if (ActionDone == false)
        {
            switch (CurrentAction)
            {
                case 1: GenerateTiles(); break;
                case 2:
                    {
                        for(int x1 = 0; x1 < 2; x1++)
                        {
                            DecideHeightMeshBiome(true, false, false);
                        }
                    }
                    break;
                case 3:
                    {
                        for (int x1 = 0; x1 < 2; x1++)
                        {
                            DecideHeightMeshBiome(false, true, false);
                        }
                    }
                    break;
                case 4:
                    {
                        for (int x1 = 0; x1 < 2; x1++)
                        {
                            DecideHeightMeshBiome(false, false, true);
                        }
                    }
                    break;
                default:
                    {
                        AddSettlementTileToList();
                        GameObject.Find("TurnManager").GetComponent<TurnManager>().SpawnUnits();
                        ActionDone = true;
                    }
                    break;
            }
        }
    }

    private void GenerateTiles()
    {
        GameObject InstTIle;
        if (CurrentIndexX % 2 == 0)
        {
            InstTIle = Instantiate(Tile, new Vector3(CurrentIndexX * HexRelativeX, 0, CurrentIndexZ * HexRelativeZ), Quaternion.identity);
            InstTIle.transform.name = "" + InstTIle.transform.position.x.ToString("0.00") + "_" + InstTIle.transform.position.z.ToString("0.00");
            AllInstTiles.Add(InstTIle);
        }
        else
        {
            InstTIle = Instantiate(Tile, new Vector3(CurrentIndexX * HexRelativeX, 0, CurrentIndexZ * HexRelativeZ + HexRelativeZ / 2), Quaternion.identity);
            InstTIle.transform.name = "" + InstTIle.transform.position.x.ToString("0.00") + "_" + InstTIle.transform.position.z.ToString("0.00");
            AllInstTiles.Add(InstTIle);
        }
        InstTIle.transform.parent = this.transform;
        CurrentIndexZ += 1;
        if (CurrentIndexZ >= InitialMapSizeZ)
        {
            CurrentIndexZ = 0;
            CurrentIndexX += 1;
            if (CurrentIndexX >= InitialMapSizeX)
            {
                CurrentAction += 1;
            }
        }
    }

    private void DecideHeightMeshBiome(bool Height, bool Mesh, bool Biome)
    {
        if (Height == true)
        {
            AllInstTiles.ToArray()[CurrentActionIndex].GetComponent<TileTypeDecider>().DecideHeight();
        }
        if (Mesh == true)
        {
            AllInstTiles.ToArray()[CurrentActionIndex].GetComponent<TileTypeDecider>().UpdateMesh();
        }
        if(Biome == true)
        {
            AllInstTiles.ToArray()[CurrentActionIndex].GetComponent<TileTypeDecider>().DecideBiome();
        }
        CurrentActionIndex += 1;
        if (CurrentActionIndex == AllInstTiles.Count)
        {
            CurrentActionIndex = 0;
            CurrentAction += 1;
        }
    }

    public IEnumerator GenTilesToRight()
    {
        WaitForSecondsRealtime SlowGenerate = new WaitForSecondsRealtime(0.01f);
        yield return SlowGenerate;
        List<GameObject> NewTiles = new List<GameObject>();
        IndexRight += 1;
        for (int x1 = IndexBottom; x1 <= IndexTop; x1++)
        {
            float Offset = IndexRight % 2 != 0 ? HexRelativeZ / 2 : 0;
            GameObject InstTile = Instantiate(Tile, new Vector3(IndexRight * HexRelativeX, 0, x1 * HexRelativeZ + Offset), Quaternion.identity);
            InstTile.transform.name = "" + InstTile.transform.position.x.ToString("0.00") + "_" + InstTile.transform.position.z.ToString("0.00");
            InstTile.transform.parent = this.transform;
            NewTiles.Add(InstTile);
            AllInstTiles.Add(InstTile);
        }
        int MiddleIndex = NewTiles.Count / 2;
        for (int x2 = 0; x2 < NewTiles.Count; x2++)
        {
            int ListIndex = MiddleIndex + x2 < NewTiles.Count ? MiddleIndex + x2 : NewTiles.Count - x2 - 1;
            NewTiles.ToArray()[ListIndex].GetComponent<TileTypeDecider>().DecideHeightAskAround();
            yield return SlowGenerate;
            NewTiles.ToArray()[ListIndex].GetComponent<TileTypeDecider>().DecideBiomeAskAround();
            yield return SlowGenerate;
        }
        foreach(GameObject x in NewTiles)
        {
            x.GetComponent<TileTypeDecider>().UpdateMeshAndAround();
            yield return SlowGenerate;
        }
        AddSettlementTileToList();
        yield break;
    }

    public IEnumerator GenTilesToLeft()
    {
        WaitForSecondsRealtime SlowGenerate = new WaitForSecondsRealtime(0.01f);
        yield return SlowGenerate;
        List<GameObject> NewTiles = new List<GameObject>();
        IndexLeft -= 1;
        for (int x1 = IndexBottom; x1 <= IndexTop; x1++)
        {
            float Offset = Mathf.Abs(IndexLeft) % 2 != 0 ? HexRelativeZ / 2 : 0;
            GameObject InstTile = Instantiate(Tile, new Vector3(IndexLeft * HexRelativeX, 0, x1 * HexRelativeZ + Offset), Quaternion.identity);
            InstTile.transform.name = "" + InstTile.transform.position.x.ToString("0.00") + "_" + InstTile.transform.position.z.ToString("0.00");
            InstTile.transform.parent = this.transform;
            NewTiles.Add(InstTile);
            AllInstTiles.Add(InstTile);
        }
        int MiddleIndex = NewTiles.Count / 2;
        for (int x2 = 0; x2 < NewTiles.Count; x2++)
        {
            int ListIndex = MiddleIndex + x2 < NewTiles.Count ? MiddleIndex + x2 : NewTiles.Count - x2 - 1;
            NewTiles.ToArray()[ListIndex].GetComponent<TileTypeDecider>().DecideHeightAskAround();
            yield return SlowGenerate;
            NewTiles.ToArray()[ListIndex].GetComponent<TileTypeDecider>().DecideBiomeAskAround();
            yield return SlowGenerate;
        }
        foreach (GameObject x in NewTiles)
        {
            x.GetComponent<TileTypeDecider>().UpdateMeshAndAround();
            yield return SlowGenerate;
        }
        AddSettlementTileToList();
        yield break;
    }

    public IEnumerator GenTilesToTop()
    {
        WaitForSecondsRealtime SlowGenerate = new WaitForSecondsRealtime(0.01f);
        yield return SlowGenerate;
        List<GameObject> NewTiles = new List<GameObject>();
        IndexTop += 1;
        for (int x1 = IndexLeft; x1 <= IndexRight; x1++)
        {
            float Offset = x1 % 2 != 0 ? HexRelativeZ / 2 : 0;
            GameObject InstTile = Instantiate(Tile, new Vector3(x1 * HexRelativeX, 0, IndexTop * HexRelativeZ + Offset), Quaternion.identity);
            InstTile.transform.name = "" + InstTile.transform.position.x.ToString("0.00") + "_" + InstTile.transform.position.z.ToString("0.00");
            InstTile.transform.parent = this.transform;
            NewTiles.Add(InstTile);
            AllInstTiles.Add(InstTile);
        }
        if (IndexLeft % 2 == 0)
        {
            for(int x2 = 0; x2 < NewTiles.Count; x2++)
            {
                if (x2 % 2 == 0)
                {
                    NewTiles.ToArray()[x2].GetComponent<TileTypeDecider>().DecideHeightAskAround();
                    yield return SlowGenerate;
                    NewTiles.ToArray()[x2].GetComponent<TileTypeDecider>().DecideBiomeAskAround();
                    yield return SlowGenerate;
                }
            }
            for (int x2 = 0; x2 < NewTiles.Count; x2++)
            {
                if (x2 % 2 != 0)
                {
                    NewTiles.ToArray()[x2].GetComponent<TileTypeDecider>().DecideHeightAskAround();
                    yield return SlowGenerate;
                    NewTiles.ToArray()[x2].GetComponent<TileTypeDecider>().DecideBiomeAskAround();
                    yield return SlowGenerate;
                }
            }
        }
        else
        {
            for (int x2 = 0; x2 < NewTiles.Count; x2++)
            {
                if (x2 % 2 != 0)
                {
                    NewTiles.ToArray()[x2].GetComponent<TileTypeDecider>().DecideHeightAskAround();
                    yield return SlowGenerate;
                    NewTiles.ToArray()[x2].GetComponent<TileTypeDecider>().DecideBiomeAskAround();
                    yield return SlowGenerate;
                }
            }
            for (int x2 = 0; x2 < NewTiles.Count; x2++)
            {
                if (x2 % 2 == 0)
                {
                    NewTiles.ToArray()[x2].GetComponent<TileTypeDecider>().DecideHeightAskAround();
                    yield return SlowGenerate;
                    NewTiles.ToArray()[x2].GetComponent<TileTypeDecider>().DecideBiomeAskAround();
                    yield return SlowGenerate;
                }
            }
        }
        foreach (GameObject x in NewTiles)
        {
            x.GetComponent<TileTypeDecider>().UpdateMeshAndAround();
            yield return SlowGenerate;
        }
        AddSettlementTileToList();
        yield break;
    }
    public IEnumerator GenTilesToBottom()
    {
        WaitForSecondsRealtime SlowGenerate = new WaitForSecondsRealtime(0.01f);
        yield return SlowGenerate;
        List<GameObject> NewTiles = new List<GameObject>();
        IndexBottom -= 1;
        for (int x1 = IndexLeft; x1 <= IndexRight; x1++)
        {
            float Offset = x1 % 2 != 0 ? HexRelativeZ / 2 : 0;
            GameObject InstTile = Instantiate(Tile, new Vector3(x1 * HexRelativeX, 0, IndexBottom * HexRelativeZ + Offset), Quaternion.identity);
            InstTile.transform.name = "" + InstTile.transform.position.x.ToString("0.00") + "_" + InstTile.transform.position.z.ToString("0.00");
            InstTile.transform.parent = this.transform;
            NewTiles.Add(InstTile);
            AllInstTiles.Add(InstTile);
        }
        if (IndexLeft % 2 != 0)
        {
            for (int x2 = 0; x2 < NewTiles.Count; x2++)
            {
                if (x2 % 2 == 0)
                {
                    NewTiles.ToArray()[x2].GetComponent<TileTypeDecider>().DecideHeightAskAround();
                    yield return SlowGenerate;
                    NewTiles.ToArray()[x2].GetComponent<TileTypeDecider>().DecideBiomeAskAround();
                    yield return SlowGenerate;
                }
            }
            for (int x2 = 0; x2 < NewTiles.Count; x2++)
            {
                if (x2 % 2 != 0)
                {
                    NewTiles.ToArray()[x2].GetComponent<TileTypeDecider>().DecideHeightAskAround();
                    yield return SlowGenerate;
                    NewTiles.ToArray()[x2].GetComponent<TileTypeDecider>().DecideBiomeAskAround();
                    yield return SlowGenerate;
                }
            }
        }
        else
        {
            for (int x2 = 0; x2 < NewTiles.Count; x2++)
            {
                if (x2 % 2 != 0)
                {
                    NewTiles.ToArray()[x2].GetComponent<TileTypeDecider>().DecideHeightAskAround();
                    yield return SlowGenerate;
                    NewTiles.ToArray()[x2].GetComponent<TileTypeDecider>().DecideBiomeAskAround();
                    yield return SlowGenerate;
                }
            }
            for (int x2 = 0; x2 < NewTiles.Count; x2++)
            {
                if (x2 % 2 == 0)
                {
                    NewTiles.ToArray()[x2].GetComponent<TileTypeDecider>().DecideHeightAskAround();
                    yield return SlowGenerate;
                    NewTiles.ToArray()[x2].GetComponent<TileTypeDecider>().DecideBiomeAskAround();
                    yield return SlowGenerate;
                }
            }
        }
        foreach (GameObject x in NewTiles)
        {
            x.GetComponent<TileTypeDecider>().UpdateMeshAndAround();
            yield return SlowGenerate;
        }
        AddSettlementTileToList();
        yield break;
    }

    private void AddSettlementTileToList()
    {
        transform.GetComponent<MapFindTile>().FindAllSettlementTiles();
    }
}

public static class MapGenScriptStatics
{
    public static GameObject MapGenObject { get; set; }
    public static Vector2[] HexRelativePositions = { new Vector2(0, 2.1317f), new Vector2(1.846f, 1.06585f), new Vector2(1.846f, -1.06585f), new Vector2(0, -2.1317f), new Vector2(-1.846f, -1.06585f), new Vector2(-1.846f, 1.06585f) };
}

public static class CanvasLink // 2.1316 2.1317
{
    public static GameObject Canvas { get; set; }
    public static GameObject HexInfoBottomObj { get; set; }
}

public static class BiomeColors
{
    public static Color SettlementColor { get; set; }
    public static List<Color> BiomeColorsList { get; set; }
}

public static class BiomeDecorations
{
    public static List<GameObject>[] BiomeDecors = new List<GameObject>[8];
}