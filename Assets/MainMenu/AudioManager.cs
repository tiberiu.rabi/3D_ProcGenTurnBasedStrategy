using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public AudioClip BackgroundMusic;
    // Start is called before the first frame update
    void Start()
    {
        transform.GetComponent<AudioSource>().Play();
    }

    private void Update()
    {
        transform.GetComponent<AudioSource>().volume = StaticSettings.MusicVolume;
    }
}
