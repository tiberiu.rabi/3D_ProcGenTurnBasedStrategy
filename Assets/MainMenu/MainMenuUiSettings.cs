using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;


public class MainMenuUiSettings : MonoBehaviour
{
    private GameObject PanelSettings;
    private GameObject PanelScoreboard;
    private GameObject PanelCredits;
    private GameObject Content;

    // Start is called before the first frame update
    void Start()
    {
        PanelSettings = transform.Find("SettingsPanel").gameObject;
        PanelScoreboard = transform.Find("ScoreboardPanel").gameObject;
        PanelCredits = transform.Find("CreditsPanel").gameObject;
        Content = PanelSettings.transform.GetChild(0).transform.GetChild(0).gameObject;
        PanelSettings.SetActive(false);
        PanelCredits.SetActive(false);
        LoadSettings();
    }

    public void LoadQuickPlay()
    {
        SceneManager.LoadScene(1);
    }

    public void ShowSettings()
    {
        PanelScoreboard.SetActive(false);
        PanelSettings.SetActive(true);
        PanelCredits.SetActive(false);
        LoadSettings();
    }

    public void LoadSettings()
    {
        LoadMusicVolumeSetting();
        LoadSoundsVolumeSetting();
        LoadEffectsVolumeSetting();
        LoadAnimationSpeedSetting();
        LoadOnQuitSetting();
    }

    public void SaveMusicVolumeSetting()
    {
        StaticSettings.MusicVolume = Content.transform.Find("MusicVSetting").GetComponent<Scrollbar>().value;
        Content.transform.Find("MusicVSetting").transform.GetChild(1).GetComponent<TMP_Text>().text = Mathf.Round(StaticSettings.MusicVolume * 100) + "%";
        PlayerPrefs.SetFloat("MusicVolume", StaticSettings.MusicVolume);
        PlayerPrefs.Save();
    }

    private void LoadMusicVolumeSetting()
    {
        StaticSettings.MusicVolume = PlayerPrefs.GetFloat("MusicVolume");
        Content.transform.Find("MusicVSetting").transform.GetChild(1).GetComponent<TMP_Text>().text = Mathf.Round(StaticSettings.MusicVolume * 100) + "%";
        Content.transform.Find("MusicVSetting").GetComponent<Scrollbar>().value = StaticSettings.MusicVolume;
    }

    public void SaveSoundsVolumeSetting()
    {
        StaticSettings.SoundsVolume = Content.transform.Find("SoundsVSetting").GetComponent<Scrollbar>().value;
        Content.transform.Find("SoundsVSetting").transform.GetChild(1).GetComponent<TMP_Text>().text = Mathf.Round(StaticSettings.SoundsVolume * 100) + "%";
        PlayerPrefs.SetFloat("SoundsVolume", StaticSettings.SoundsVolume);
        PlayerPrefs.Save();
    }

    private void LoadSoundsVolumeSetting()
    {
        StaticSettings.SoundsVolume = PlayerPrefs.GetFloat("SoundsVolume");
        Content.transform.Find("SoundsVSetting").transform.GetChild(1).GetComponent<TMP_Text>().text = Mathf.Round(StaticSettings.SoundsVolume * 100) + "%";
        Content.transform.Find("SoundsVSetting").GetComponent<Scrollbar>().value = StaticSettings.SoundsVolume;
    }

    public void SaveEffectsVolumeSetting()
    {
        StaticSettings.SoundsVolume = Content.transform.Find("EffectsVSetting").GetComponent<Scrollbar>().value;
        Content.transform.Find("EffectsVSetting").transform.GetChild(1).GetComponent<TMP_Text>().text = Mathf.Round(StaticSettings.SoundsVolume * 100) + "%";
        PlayerPrefs.SetFloat("EffectsVolume", StaticSettings.SoundsVolume);
        PlayerPrefs.Save();
    }

    private void LoadEffectsVolumeSetting()
    {
        StaticSettings.SoundsVolume = PlayerPrefs.GetFloat("EffectsVolume");
        Content.transform.Find("EffectsVSetting").transform.GetChild(1).GetComponent<TMP_Text>().text = Mathf.Round(StaticSettings.SoundsVolume * 100) + "%";
        Content.transform.Find("EffectsVSetting").GetComponent<Scrollbar>().value = StaticSettings.SoundsVolume;
    }

    public void SaveAnimationSpeedSetting()
    {
        switch (Content.transform.Find("AnimSpeedSetting").GetComponent<Scrollbar>().value)
        {
            case <= 0.125f: StaticSettings.AnimSpeedMultiplier = 0.75f; break;
            case > 0.125f and < 0.375f: StaticSettings.AnimSpeedMultiplier = 1f; break;
            case >= 0.375f and <= 0.625f: StaticSettings.AnimSpeedMultiplier = 1.25f; break;
            case > 0.625f and < 0.875f: StaticSettings.AnimSpeedMultiplier = 1.5f; break;
            case >= 0.875f: StaticSettings.AnimSpeedMultiplier = 2f; break;
            default: StaticSettings.AnimSpeedMultiplier = 1f; break;
        }
        Content.transform.Find("AnimSpeedSetting").transform.GetChild(1).transform.GetComponent<TMP_Text>().text = "x" + StaticSettings.AnimSpeedMultiplier.ToString("0.##");
        PlayerPrefs.SetFloat("AnimSpeedSetting", Content.transform.Find("AnimSpeedSetting").GetComponent<Scrollbar>().value);
    }

    private void LoadAnimationSpeedSetting()
    {
        Content.transform.Find("AnimSpeedSetting").GetComponent<Scrollbar>().value = PlayerPrefs.GetFloat("AnimSpeedSetting");
        switch (Content.transform.Find("AnimSpeedSetting").GetComponent<Scrollbar>().value)
        {
            case <= 0.125f: StaticSettings.AnimSpeedMultiplier = 0.75f; break;
            case > 0.125f and < 0.375f: StaticSettings.AnimSpeedMultiplier = 1f; break;
            case >= 0.375f and <= 0.625f: StaticSettings.AnimSpeedMultiplier = 1.25f; break;
            case > 0.625f and < 0.875f: StaticSettings.AnimSpeedMultiplier = 1.5f; break;
            case >= 0.875f: StaticSettings.AnimSpeedMultiplier = 2f; break;
            default: StaticSettings.AnimSpeedMultiplier = 1f; break;
        }
        Content.transform.Find("AnimSpeedSetting").transform.GetChild(1).transform.GetComponent<TMP_Text>().text = "x" + StaticSettings.AnimSpeedMultiplier.ToString("0.##");
    }

    public void SaveOnQuitSetting()
    {
        StaticSettings.SaveOnQuit = Content.transform.Find("SaveOptions").transform.Find("Option1").GetComponent<Toggle>().isOn;
        PlayerPrefs.SetInt("SaveOnQuit", StaticSettings.SaveOnQuit == true ? 1 : 0);
        Debug.LogError(StaticSettings.SaveOnQuit == true ? 1 : 0);
        PlayerPrefs.Save();
    }

    private void LoadOnQuitSetting()
    {
        Content.transform.Find("SaveOptions").GetComponent<ToggleGroup>().enabled = false;
        StaticSettings.SaveOnQuit = PlayerPrefs.GetInt("SaveOnQuit") == 1 ? true : false;
        Content.transform.Find("SaveOptions").transform.Find("Option1").GetComponent<Toggle>().isOn = StaticSettings.SaveOnQuit;
        Content.transform.Find("SaveOptions").transform.Find("Option2").GetComponent<Toggle>().isOn = !StaticSettings.SaveOnQuit;
        Content.transform.Find("SaveOptions").GetComponent<ToggleGroup>().enabled = true;
    }

    public void ShowCredits()
    {
        PanelScoreboard.SetActive(false);
        PanelSettings.SetActive(false);
        PanelCredits.SetActive(true);
    }

    public void ShowScoreboard()
    {
        PanelScoreboard.SetActive(true);
        UpdateScoreboard();
        PanelSettings.SetActive(false);
        PanelCredits.SetActive(false);
    }

    public void UpdateScoreboard()
    {
        //update scoreboard
    }

    public void ResetScoreboard()
    {

    }

    public void QuitApplication()
    {
        Application.Quit();
    }
}

public static class StaticSettings
{
    public static float MusicVolume { get; set; }
    public static float SoundsVolume { get; set; }
    public static float VFXVolume { get; set; }
    public static float AnimSpeedMultiplier { get; set; }
    public static bool SaveOnQuit { get; set; }
}